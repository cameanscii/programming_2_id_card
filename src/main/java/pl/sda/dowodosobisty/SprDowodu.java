package pl.sda.dowodosobisty;



public class SprDowodu {
    public boolean czyNrISeriaPoprawne() {
        String dowod="ABS123456";

        final int[] FACTORS ={7,3,1,9,7,3,1,7,3};

        if (dowod==null||dowod.length()!=9){
            return false;
        }
        String[] tablicaSlow = dowod.split("");
        char[] charTab=dowod.toCharArray();

        int[] tablicaIloczynow=new int[9];
        int sumaIloczynow=0;

        for (int i = 0; i <3 ; i++) {
            tablicaIloczynow[i]=charTab[i]-55;
        }

        for (int i = 3; i <9 ; i++) {
            tablicaIloczynow[i]=Integer.parseInt(tablicaSlow[i]);
        }


        for (int i = 0; i <9 ; i++) {
            sumaIloczynow+=tablicaIloczynow[i]*FACTORS[i];
        }

        if (sumaIloczynow%10==0){
            return true;
        }

    return false;
    }
}
